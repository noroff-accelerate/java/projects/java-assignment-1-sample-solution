package no.accelerate.exceptions;

/**
 * Custom exception for all armor related validation.
 */
public class InvalidArmorException extends RuntimeException {
    public InvalidArmorException(String message) {
        super(message);
    }
}
