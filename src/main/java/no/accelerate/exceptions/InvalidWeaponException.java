package no.accelerate.exceptions;

/**
 * Custom exception for all weapon related validation.
 */
public class InvalidWeaponException extends RuntimeException {
    public InvalidWeaponException(String message) {
        super(message);
    }
}
