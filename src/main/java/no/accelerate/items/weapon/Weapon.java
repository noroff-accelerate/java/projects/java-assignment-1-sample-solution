package no.accelerate.items.weapon;

import no.accelerate.items.Item;
import no.accelerate.items.Slot;

/**
 * Item that can be equipped in the weapon slot.
 * Weapon provides flat damage to the Hero and is the basis for damage scaling.
 */
public class Weapon extends Item {

    private double damage;
    private WeaponType weaponType;

    public Weapon(String name, int requiredLevel, int damage, WeaponType weaponType) {
        // Weapons are always in the weapon slot
        super(name, Slot.WEAPON, requiredLevel);
        this.damage = damage;
        this.weaponType = weaponType;
    }

    public double getDamage() {
        return damage;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }
}
