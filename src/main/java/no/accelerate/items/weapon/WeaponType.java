package no.accelerate.items.weapon;

/**
 * Encapsulates all the types of weapons.
 */
public enum WeaponType {
    AXE,
    BOW,
    DAGGER,
    HAMMER,
    STAFF,
    SWORD,
    WAND
}
