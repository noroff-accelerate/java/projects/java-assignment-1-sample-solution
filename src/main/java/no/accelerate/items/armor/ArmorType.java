package no.accelerate.items.armor;

/**
 * Encapsulates all the types of armor.
 */
public enum ArmorType {
    PLATE,
    MAIL,
    LEATHER,
    CLOTH
}
