package no.accelerate.items.armor;

import no.accelerate.items.Item;
import no.accelerate.items.Slot;
import no.accelerate.util.Attributes;

/**
 * Item that can be equipped in non-weapon slots.
 * Armor provides bonus attributes to the equipping Hero.
 */
public class Armor extends Item {
    private Attributes bonusAttributes;
    private ArmorType armorType;

    public Armor(String name, Slot slot, int requiredLevel, Attributes bonusAttributes, ArmorType armorType) {
        super(name, slot, requiredLevel);
        this.bonusAttributes = bonusAttributes;
        this.armorType = armorType;
    }

    public Attributes getBonusAttributes() {
        return bonusAttributes;
    }

    public ArmorType getArmorType() {
        return armorType;
    }
}
