package no.accelerate.items;

/**
 * Encapsulates all the slots items can be equipped in.
 */
public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
