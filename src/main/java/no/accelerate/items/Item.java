package no.accelerate.items;

/**
 * Abstraction for all items. It holds shared fields and methods.
 */
public abstract class Item {
    private String name;
    private Slot slot;
    private int requiredLevel;

    public Item(String name, Slot slot, int requiredLevel) {
        this.name = name;
        this.slot = slot;
        this.requiredLevel = requiredLevel;
    }

    public String getName() {
        return name;
    }

    public Slot getSlot() {
        return slot;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }
}
