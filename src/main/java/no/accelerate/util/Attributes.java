package no.accelerate.util;

import java.util.Objects;

/**
 * Encapsulates all the attributes for a Hero.
 * This is used for both level attributes and bonus attributes on armor.
 */
public class Attributes {
    private int strength;
    private int dexterity;
    private int intelligence;

    public Attributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * Increases the current attributes by the passed amount.
     * @param attributes Attributes object to increase the current instance by.
     */
    public void increase(Attributes attributes) {
        this.strength += attributes.strength;
        this.dexterity += attributes.dexterity;
        this.intelligence += attributes.intelligence;
    }

    /**
     * Increases the current attributes by the passed amounts.
     * @param strength Amount to increase strength by.
     * @param dexterity Amount to increase dexterity by.
     * @param intelligence Amount to increase intelligence by.
     */
    public void increase(int strength, int dexterity, int intelligence) {
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attributes that = (Attributes) o;
        return strength == that.strength && dexterity == that.dexterity && intelligence == that.intelligence;
    }

    @Override
    public int hashCode() {
        return Objects.hash(strength, dexterity, intelligence);
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }
}
