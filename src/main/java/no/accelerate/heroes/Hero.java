package no.accelerate.heroes;

import no.accelerate.exceptions.InvalidArmorException;
import no.accelerate.exceptions.InvalidWeaponException;
import no.accelerate.items.Item;
import no.accelerate.items.Slot;
import no.accelerate.items.armor.Armor;
import no.accelerate.items.armor.ArmorType;
import no.accelerate.items.weapon.Weapon;
import no.accelerate.items.weapon.WeaponType;
import no.accelerate.util.Attributes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Abstraction for all heroes in the game. Holds shared functionality and can be overridden by children.
 */
public abstract class Hero {
    // Want to encapsulate state. So external classes cant change it.
    // We want some aspects to be mutated by the children, so we change it to protected.
    private String name; // Most likely going to be "John"
    private int level = 1; // Starts at 1
    protected List<WeaponType> validWeaponTypes; // Defined by the children
    protected List<ArmorType> validArmorTypes; // Defined by the children
    protected Attributes levelAttributes; // Initial values set by children. Then the children increase as they level up.
    private Map<Slot, Item> equipment;

    public Hero(String name) {
        this.name = name;
        initializeEquipment();
    }

    private void initializeEquipment() {
        equipment = new HashMap<>();
        equipment.put(Slot.HEAD, null);
        equipment.put(Slot.BODY,null);
        equipment.put(Slot.LEGS,null);
        equipment.put(Slot.WEAPON,null);
    }

    /**
     * Increases the level of the Hero by 1 and increases their level attributes based on the subclass.
     */
    public void levelUp() {
        level++;
    }

    /**
     * Attempts to equip a weapon. Throws exceptions if the level is too high
     * or the weapon is the wrong type for the subclass.
     * @throws InvalidWeaponException
     * @param weapon Weapon to equip.
     */
    public void equip(Weapon weapon) {
        if(weapon.getRequiredLevel() > level)
            throw new InvalidWeaponException("Weapon level too high");
        if(!validWeaponTypes.contains(weapon.getWeaponType()))
            throw new InvalidWeaponException("Weapon is the wrong type");
        equipment.put(weapon.getSlot(), weapon); // Can also just say .put(Slot.WEAPON, weapon)
    }

    /**
     * Attempts to equip armor. Throws exceptions if the level is too high
     * or the armor is the wrong type for the subclass.
     * @param armor Armor to equip
     * @throws InvalidArmorException
     */
    public void equip(Armor armor) {
        if(armor.getRequiredLevel() > level)
            throw new InvalidArmorException("Armor level too high");
        if(!validArmorTypes.contains(armor.getArmorType()))
            throw new InvalidArmorException("Armor is the wrong type");
        equipment.put(armor.getSlot(), armor);
    }

    /**
     * Calculates the Heroes total attributes.
     * It's an aggregation of all the bonus attributes from equipped
     * armor and the attributes from the heroes level.
     * @return Total attributes
     */
    public Attributes getTotalAttributes() {
        // Use current attributes from level as the base
        Attributes total = levelAttributes;
        // Loop through all armor and increase total by that amount
        for (Map.Entry<Slot, Item> entry: equipment.entrySet()) {
            if(entry.getKey() == Slot.WEAPON || entry.getValue() == null)
                // Skip the weapon or if nothing is equipped
                continue;
            // Take the item, cast it to armor to get the bonus attributes.
            // Then increase our total by the bonus attribute
            total.increase(((Armor)entry.getValue()).getBonusAttributes());
        }
        return total;
    }

    /**
     * Calculates the damage a hero can deal.
     * This is calculated based on the equipped weapons damage (set to 1 if no weapon is equipped)
     * and a specific attribute from the Heroes total attributes. <br/><br/>
     * Ex. Warrior uses strength to deal more damage.
     * @return Damage a Hero can deal.
     */
    public double getDamage() {
        // Need to get weapon damage. Defaults to 1 if we don't have a weapon equipped.
        double wepDam = 1;
        // We check if there is a weapon equipped, then use its damage. Need to cast it to weapon first.
        if(equipment.get(Slot.WEAPON) != null)
            wepDam = ((Weapon)equipment.get(Slot.WEAPON)).getDamage();
        // Defer the getDamagingAttribute to the children.
        return wepDam * (1 + (getDamagingAttribute(getTotalAttributes())/100d));
    }

    /**
     * Gets the damaging attribute from the total attributes.
     * @param totalAttributes
     * @return The field that relates to damage for the subclass.
     *
     * <br/><br/> Ex. Warrior will return the strength portion of total attributes.
     */
    protected abstract int getDamagingAttribute(Attributes totalAttributes);

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }
}
