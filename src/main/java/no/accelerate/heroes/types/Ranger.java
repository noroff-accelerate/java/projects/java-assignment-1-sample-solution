package no.accelerate.heroes.types;

import no.accelerate.heroes.Hero;
import no.accelerate.items.armor.ArmorType;
import no.accelerate.items.weapon.WeaponType;
import no.accelerate.util.Attributes;

import java.util.Arrays;

/**
 * Dexterity based subclass of Hero.
 */
public class Ranger extends Hero {
    public Ranger(String name) {
        super(name);
        levelAttributes = new Attributes(1,7,1);
        validArmorTypes = Arrays.asList(ArmorType.LEATHER, ArmorType.MAIL);
        validWeaponTypes = Arrays.asList(WeaponType.BOW);
    }

    @Override
    public void levelUp() {
        levelAttributes.increase(1,5,1);
        super.levelUp();
    }

    @Override
    protected int getDamagingAttribute(Attributes totalAttributes) {
        return totalAttributes.getDexterity();
    }
}
