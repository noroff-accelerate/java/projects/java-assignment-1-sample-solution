package no.accelerate.heroes.types;

import no.accelerate.heroes.Hero;
import no.accelerate.items.armor.ArmorType;
import no.accelerate.items.weapon.WeaponType;
import no.accelerate.util.Attributes;

import java.util.Arrays;

/**
 * Intelligence based subclass of Hero.
 */
public class Mage extends Hero {
    public Mage(String name) {
        super(name);
        levelAttributes = new Attributes(1,1,8);
        validArmorTypes = Arrays.asList(ArmorType.CLOTH);
        validWeaponTypes = Arrays.asList(WeaponType.STAFF, WeaponType.WAND);
    }

    @Override
    public void levelUp() {
        levelAttributes.increase(1,1,5);
        super.levelUp();
    }

    @Override
    protected int getDamagingAttribute(Attributes totalAttributes) {
        return totalAttributes.getIntelligence();
    }
}
