package no.accelerate.heroes.types;

import no.accelerate.heroes.Hero;
import no.accelerate.items.armor.ArmorType;
import no.accelerate.items.weapon.WeaponType;
import no.accelerate.util.Attributes;

import java.util.Arrays;

/**
 * Strength based subclass of Hero.
 */
public class Warrior extends Hero {
    public Warrior(String name) {
        super(name);
        levelAttributes = new Attributes(5,2,1);
        validArmorTypes = Arrays.asList(ArmorType.MAIL, ArmorType.PLATE);
        validWeaponTypes = Arrays.asList(WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD);
    }

    @Override
    public void levelUp() {
        levelAttributes.increase(3,2,1);
        super.levelUp();
    }

    @Override
    protected int getDamagingAttribute(Attributes totalAttributes) {
        return totalAttributes.getStrength();
    }
}
