package no.accelerate.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Attr;

import static org.junit.jupiter.api.Assertions.*;

class AttributesTest {
    Attributes attributes;

    @BeforeEach
    void setUp() {
        attributes = new Attributes(1,2,3);
    }

    @Test
    public void constructor_shouldHaveCorrectStrength() {
        // Arrange
        int expected = 1;
        // Act
        int actual = attributes.getStrength();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void constructor_shouldHaveCorrectDexterity() {
        // Arrange
        int expected = 2;
        // Act
        int actual = attributes.getDexterity();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void constructor_shouldHaveCorrectIntelligence() {
        // Arrange
        int expected = 3;
        // Act
        int actual = attributes.getIntelligence();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void increase_shouldHaveCorrectStrengthUsingIntParam() {
        // Arrange
        int expected = 1+1;
        // Act
        attributes.increase(1,0,0);
        int actual = attributes.getStrength();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void increase_shouldHaveCorrectStrengthUsingAttributesParam() {
        // Arrange
        int expected = 1+1;
        // Act
        attributes.increase(new Attributes(1,0,0));
        int actual = attributes.getStrength();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void increase_shouldHaveCorrectDexterityUsingIntParam() {
        // Arrange
        int expected = 2+1;
        // Act
        attributes.increase(0,1,0);
        int actual = attributes.getDexterity();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void increase_shouldHaveCorrectDexterityUsingAttributesParam() {
        // Arrange
        int expected = 2+1;
        // Act
        attributes.increase(new Attributes(0,1,0));
        int actual = attributes.getDexterity();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void increase_shouldHaveCorrectIntelligenceUsingIntParam() {
        // Arrange
        int expected = 3+1;
        // Act
        attributes.increase(0,0,1);
        int actual = attributes.getIntelligence();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void increase_shouldHaveCorrectIntelligenceUsingAttributesParam() {
        // Arrange
        int expected = 3+1;
        // Act
        attributes.increase(new Attributes(0,0,1));
        int actual = attributes.getIntelligence();
        // Assert
        assertEquals(expected,actual);
    }
}