package no.accelerate.items;

import no.accelerate.items.weapon.Weapon;
import no.accelerate.items.weapon.WeaponType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    Item testItem;

    @BeforeEach
    void setUp() {
        testItem = new Weapon("TestItem",1,1, WeaponType.AXE);
    }

    @Test
    void constructor_shouldHaveCorrectName() {
        // Arrange
        String expected = "TestItem";
        // Act
        String actual = testItem.getName();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void constructor_shouldHaveCorrectRequiredLevel() {
        // Arrange
        int expected = 1;
        // Act
        int actual = testItem.getRequiredLevel();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void constructor_shouldHaveCorrectSlot() {
        // Arrange
        Slot expected = Slot.WEAPON;
        // Act
        Slot actual = testItem.getSlot();
        // Assert
        assertEquals(expected,actual);
    }
}