package no.accelerate.items.armor;

import no.accelerate.items.Slot;
import no.accelerate.util.Attributes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    Armor testArmor;

    @BeforeEach
    void setUp() {
        testArmor = new Armor(
                "TestArmor",
                Slot.BODY,
                1,
                new Attributes(1,1,1),
                ArmorType.PLATE
        );
    }

    @Test
    void constructor_shouldHaveCorrectAttributes() {
        // Arrange
        Attributes expected = new Attributes(1,1,1);
        // Act
        Attributes actual = testArmor.getBonusAttributes();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void constructor_shouldHaveCorrectArmorType() {
        // Arrange
        ArmorType expected = ArmorType.PLATE;
        // Act
        ArmorType actual = testArmor.getArmorType();
        // Assert
        assertEquals(expected,actual);
    }
}