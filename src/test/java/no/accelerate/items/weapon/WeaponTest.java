package no.accelerate.items.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WeaponTest {
    Weapon testWeapon;

    @BeforeEach
    void setUp() {
        testWeapon = new Weapon("TestWeapon",1,1, WeaponType.AXE);
    }

    @Test
    void constructor_shouldHaveCorrectDamage() {
        // Arrange
        double expected = 1.0; // can also say 1d to force it as a double.
        // Act
        double actual = testWeapon.getDamage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void constructor_shouldHaveCorrectWeaponType() {
        // Arrange
        WeaponType expected = WeaponType.AXE; // can also say 1d to force it as a double.
        // Act
        WeaponType actual = testWeapon.getWeaponType();
        // Assert
        assertEquals(expected,actual);
    }
}