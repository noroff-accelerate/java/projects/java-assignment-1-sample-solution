package no.accelerate.heroes;

import no.accelerate.heroes.types.Mage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    Hero testHero;
    @BeforeEach
    void setUp() {
        testHero = new Mage("Test");
    }

    @Test
    public void constructor_shouldHaveCorrectName() {
        // Arrange
        String expected = "Test";
        // Act
        String actual = testHero.getName();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void constructor_shouldBeLevel1() {
        // Arrange
        int expected = 1;
        // Act
        int actual = testHero.getLevel();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void levelUp_shouldBeLevel2() {
        // Arrange
        int expected = 1+1;
        // Act
        testHero.levelUp();
        int actual = testHero.getLevel();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void constructor_noEquipment_shouldHaveCorrectDamage() {
        // Arrange
        double expected = 1d * (1 + 8d/100d);
        // Act
        double actual = testHero.getDamage();
        // Assert
        assertEquals(expected,actual);
    }
}