package no.accelerate.heroes;

import no.accelerate.exceptions.InvalidArmorException;
import no.accelerate.exceptions.InvalidWeaponException;
import no.accelerate.heroes.types.Warrior;
import no.accelerate.items.Slot;
import no.accelerate.items.armor.Armor;
import no.accelerate.items.armor.ArmorType;
import no.accelerate.items.weapon.Weapon;
import no.accelerate.items.weapon.WeaponType;
import no.accelerate.util.Attributes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EquipmentTest {
    Warrior testWarrior;
    Armor invalidArmor, validArmor;
    Weapon invalidWeapon, validWeapon;

    @BeforeEach
    void setUp() {
        testWarrior = new Warrior("TestWarrior");
    }

    @Test
    void equip_invalidArmorLevelTooHigh_shouldThrowArmorExceptionWithProperMessage() {
        // Arrange
        invalidArmor = new Armor("InvalidArmor",
                Slot.BODY,
                2,
                new Attributes(1,2,3),
                ArmorType.PLATE);
        String expected = "Armor level too high";
        // Act
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testWarrior.equip(invalidArmor));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void equip_invalidArmorWrongArmorType_shouldThrowArmorExceptionWithProperMessage() {
        // Arrange
        invalidArmor = new Armor("InvalidArmor",
                Slot.BODY,
                1,
                new Attributes(1,2,3),
                ArmorType.CLOTH);
        String expected = "Armor is the wrong type";
        // Act
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testWarrior.equip(invalidArmor));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void equip_invalidWeaponLevelTooHigh_shouldThrowWeaponExceptionWithProperMessage() {
        // Arrange
        invalidWeapon = new Weapon("InvalidWeapon",
                2,
                1,
                WeaponType.AXE);
        String expected = "Weapon level too high";
        // Act
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testWarrior.equip(invalidWeapon));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void equip_invalidWeaponWrongType_shouldThrowWeaponExceptionWithProperMessage() {
        // Arrange
        invalidWeapon = new Weapon("InvalidWeapon",
                1,
                1,
                WeaponType.WAND);
        String expected = "Weapon is the wrong type";
        // Act
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testWarrior.equip(invalidWeapon));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void equip_validWeapon_shouldHaveCorrectDamage() {
        // Arrange
        validWeapon = new Weapon("ValidWeapon",
                1,
                2,
                WeaponType.AXE);
        double expected = 2 * (1 + 5d/100d); // The d is to force it as a double, so it doesn't truncate.
        // Act
        testWarrior.equip(validWeapon);
        double actual = testWarrior.getDamage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void equip_validArmor_shouldHaveCorrectDamage() {
        // Arrange
        validArmor = new Armor("ValidArmor",
                Slot.BODY,
                1,
                new Attributes(1,1,1),
                ArmorType.PLATE);
        double expected = 1 * (1 + (5 + 1)/100d); // The d is to force it as a double, so it doesn't truncate.
        // Act
        testWarrior.equip(validArmor);
        double actual = testWarrior.getDamage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void equip_validArmorAndWeapon_shouldHaveCorrectDamage() {
        // Arrange
        validArmor = new Armor("ValidArmor",
                Slot.BODY,
                1,
                new Attributes(1,1,1),
                ArmorType.PLATE);
        validWeapon = new Weapon("ValidWeapon",
                1,
                2,
                WeaponType.AXE);
        double expected = 2 * (1 + (5 + 1)/100d); // The d is to force it as a double, so it doesn't truncate.
        // Act
        testWarrior.equip(validWeapon);
        testWarrior.equip(validArmor);
        double actual = testWarrior.getDamage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void equip_replacedValidArmor_shouldHaveCorrectDamage() {
        // Arrange
        validArmor = new Armor("ValidArmor",
                Slot.BODY,
                1,
                new Attributes(1,1,1),
                ArmorType.PLATE);
        Armor replaceArmor = new Armor("ReplaceArmor",
                Slot.BODY,
                1,
                new Attributes(2,1,1),
                ArmorType.PLATE);
        double expected = 1 * (1 + (5 + 2)/100d); // The d is to force it as a double, so it doesn't truncate.
        // Act
        testWarrior.equip(validArmor);
        testWarrior.equip(replaceArmor);
        double actual = testWarrior.getDamage();
        // Assert
        assertEquals(expected,actual);
    }
}
