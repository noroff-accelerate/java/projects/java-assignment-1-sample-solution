package no.accelerate.heroes.types;

import no.accelerate.heroes.Hero;
import no.accelerate.util.Attributes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    Hero ranger;

    @BeforeEach
    void setUp() {
        ranger = new Ranger("Test");
    }

    @Test
    public void constructor_shouldHaveCorrectStartingAttributes() {
        // Arrange
        Attributes expected = new Attributes(1,7,1);
        // Act
        Attributes actual = ranger.getTotalAttributes();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void levelUp_shouldHaveCorrectAttributes() {
        // Arrange
        Attributes expected = new Attributes(1+1,7+5,1+1);
        // Act
        ranger.levelUp();
        Attributes actual = ranger.getTotalAttributes();
        // Assert
        assertEquals(expected,actual);
    }
}