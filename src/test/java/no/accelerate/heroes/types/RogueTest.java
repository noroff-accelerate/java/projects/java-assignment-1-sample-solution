package no.accelerate.heroes.types;

import no.accelerate.heroes.Hero;
import no.accelerate.util.Attributes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    Hero rogue;
    @BeforeEach
    void setUp() {
        rogue = new Rogue("Test");
    }

    @Test
    public void constructor_shouldHaveCorrectStartingAttributes() {
        // Arrange
        Attributes expected = new Attributes(2,6,1);
        // Act
        Attributes actual = rogue.getTotalAttributes();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void levelUp_shouldHaveCorrectAttributes() {
        // Arrange
        Attributes expected = new Attributes(2+1,6+4,1+1);
        // Act
        rogue.levelUp();
        Attributes actual = rogue.getTotalAttributes();
        // Assert
        assertEquals(expected,actual);
    }
}