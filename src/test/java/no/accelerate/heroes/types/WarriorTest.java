package no.accelerate.heroes.types;

import no.accelerate.heroes.Hero;
import no.accelerate.util.Attributes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    Hero warrior;
    @BeforeEach
    void setUp() {
        warrior = new Warrior("Test");
    }

    @Test
    public void constructor_shouldHaveCorrectStartingAttributes() {
        // Arrange
        Attributes expected = new Attributes(5,2,1);
        // Act
        Attributes actual = warrior.getTotalAttributes();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void levelUp_shouldHaveCorrectAttributes() {
        // Arrange
        Attributes expected = new Attributes(5+3,2+2,1+1);
        // Act
        warrior.levelUp();
        Attributes actual = warrior.getTotalAttributes();
        // Assert
        assertEquals(expected,actual);
    }
}