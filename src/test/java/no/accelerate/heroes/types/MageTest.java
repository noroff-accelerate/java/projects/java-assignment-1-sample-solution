package no.accelerate.heroes.types;

import no.accelerate.heroes.Hero;
import no.accelerate.util.Attributes;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    Hero mage;

    @BeforeEach
    void setUp() {
        mage = new Mage("Test");
    }

    @Test
    public void constructor_shouldHaveCorrectStartingAttributes() {
        // Arrange
        Attributes expected = new Attributes(1,1,8);
        // Act
        Attributes actual = mage.getTotalAttributes();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void levelUp_shouldHaveCorrectAttributes() {
        // Arrange
        Attributes expected = new Attributes(1+1,1+1,8+5);
        // Act
        mage.levelUp();
        Attributes actual = mage.getTotalAttributes();
        // Assert
        assertEquals(expected,actual);
    }

}